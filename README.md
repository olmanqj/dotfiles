# dotfiles


## Set up
In case _$HOME_ is empty.
~~~
git init --bare $HOME/.dotfiles
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles remote add origin git@gitlab.com:olmanqj/dotfiles.git
~~~


## Replication
Iin case _$HOME_ is not empty.
~~~
git clone --separate-git-dir=$HOME/.dotfiles https://gitlab.com/olmanqj/dotfiles.git dotfiles-tmp
rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/
rm --recursive dotfiles-tmp

~~~






---

## References

- https://pgaskin.net/posts/git-dotfiles/
- https://github.com/Siilwyn/my-dotfiles/tree/master/.my-dotfiles
